//
//  ViewController.swift
//  UIKitStoryboard
//
//  Created by Reza Harris on 23/03/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Hello World")
        printHello()
        hola()
        // Do any additional setup after loading the view.
    }
    
    private func printHello() {
        print("Hello World")
    }
    
    private func hola() {
        print("Hola!!")
    }


}

